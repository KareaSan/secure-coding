import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Log {
    @PrimaryGeneratedColumn("uuid")
    "id": string;

    @Column()
    "type": string;

    @Column()
    "pid": string;

    @Column()
    "date": Date;

    @Column()
    "level": number;

    @Column({ nullable: true })
    "error": number;

    @Column({ nullable: true })
    "method": string;

    @Column({ nullable: true })
    "url": string;

    @Column()
    "msg": string;

    @Column({ nullable: true })
    "resStatusCode": number;
}
