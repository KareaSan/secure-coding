import * as fs from 'fs';

export function writeLogs(data: string) {
    const directory = './src/logs';
    if (!fs.existsSync(directory)) {
        fs.mkdirSync(directory);
    }

    fs.writeFile(`${directory}/logs.txt`, (data + "\n"), { flag: 'a+' }, (err) => {
        if (err) {
        console.error(`Error writing logs: ${err}`);
        } else {
        console.log('Logs written to file.');
        }
    });
}