import { Logger } from 'typeorm';

export class MyLogger implements Logger {

    async logQuery(query: string, parameters?: any[]) {
        console.log(`Query: ${query}`);
        if (parameters) {
        console.log(`Parameters: ${JSON.stringify(parameters)}`);
        }
    }

    logQueryError(error: string, query: string, parameters?: any[]) {
        console.log(`Query error: ${error}`);
        console.log(`Query: ${query}`);
        if (parameters) {
        console.log(`Parameters: ${JSON.stringify(parameters)}`);
        }
    }

    logQuerySlow(time: number, query: string, parameters?: any[]) {
        console.log(`Query took ${time} ms to execute`);
        console.log(`Query: ${query}`);
        if (parameters) {
        console.log(`Parameters: ${JSON.stringify(parameters)}`);
        }
    }

    logSchemaBuild(message: string) {
        console.log(`Schema build: ${message}`);
    }

    logMigration(message: string) {
        console.log(`Migration: ${message}`);
    }

    log(level: 'log' | 'info' | 'warn', message: any) {
        switch (level) {
        case 'log':
            console.log(`Log: ${message}`);
            break;
        case 'info':
            console.log(`Info: ${message}`);
            break;
        case 'warn':
            console.log(`Warning: ${message}`);
            break;
        }
    }
}
