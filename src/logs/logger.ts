import { pino } from "pino";
import { writeLogs } from "../utils";
import { AppDataSource } from "../lib/typeorm";
import { Log } from "../entities/log";

const { Writable } = require('stream');
const repoLog = AppDataSource.getRepository(Log);

// create a writable stream to capture logs
const writableStream = new Writable({
  async write(chunk: string, encoding: any, callback: () => void) {
    let jsonObject = JSON.parse(chunk);
    writeLogs(JSON.stringify(jsonObject));

    const log = new Log();
    if('err' in jsonObject){
      log.type= "err";
      log.date= new Date(jsonObject.time);
      log.pid= jsonObject.pid;
      log.level= jsonObject.level;
      log.msg= jsonObject.err.message;
      log.error= jsonObject.err.type;
      log.resStatusCode= jsonObject.err.statusCode;
      await repoLog.save(log);
    } else if('req' in jsonObject) {
      log.type= "req";
      log.date= new Date(jsonObject.time);
      log.pid= jsonObject.pid;
      log.level= jsonObject.level;
      log.method= jsonObject.req.method;
      log.url= jsonObject.req.url;
      log.msg= jsonObject.msg;
      await repoLog.save(log);
    } else if('res' in jsonObject) {
      log.type= "res";
      log.date= new Date(jsonObject.time);
      log.pid= jsonObject.pid;
      log.level= jsonObject.level;
      log.msg= jsonObject.msg;
      log.resStatusCode= jsonObject.res.statusCode;
      await repoLog.save(log);
    } else {
      log.type= "info";
      log.date= new Date(jsonObject.time);
      log.pid= jsonObject.pid;
      log.level= jsonObject.level;
      log.msg= jsonObject.msg;
      await repoLog.save(log);
    }
    callback();
  },
});

// create a Pino logger that writes to the stream
const logger = pino({ level: 'debug' }, writableStream);

export default logger;